package com.rphdevelopers.ritsports

class Const {
    val LOGIN_URL : String = "https://ritgames.000webhostapp.com/ritsports/login.php"
    val DEPARTMENT_URL : String = "https://ritgames.000webhostapp.com/ritsports/getdepartments.php"

    val ROLE_ADMIN = "admin"
    val ROLE_TEAM_MANAGER = "team_manager"
    val ROLE_TOUR_COORDINATOR = "tournament_coordinator"
    val ROLE_SCORE_UPDATER = "scoreupdater"

    val EMAIL_SHARED_PREF = "email"
    val LOGGEDIN_SHARED_PREF = "loggedin"
    val LOGGEDIN_ROLE_SHARED_PREF = "role"
    val SHARED_PREF_NAME = "tech"

}