package com.rphdevelopers.ritsports

import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.get
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.json.JSONArray
import org.json.JSONObject



private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class fragment_viewgame : Fragment(), AdapterView.OnItemSelectedListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var btnAddCoordinator: Button
    lateinit var etUName: EditText
    lateinit var etPassword: EditText
    lateinit var etConfirmPassword: EditText
    lateinit var etEmail: EditText
    lateinit var spDepartment: Spinner
    lateinit var const: Const
    lateinit var rootView: View
    lateinit var department: String
    lateinit var tilCPassword: TextInputLayout

    var departments = ArrayList<String>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_add_coordinator, container, false)

        btnAddCoordinator = rootView.findViewById(R.id.btn_add_coordinator)
        spDepartment = rootView.findViewById(R.id.spDepartment)
        tilCPassword = rootView.findViewById(R.id.tilCPassword)
        etUName = rootView.findViewById(R.id.etUname)
        etEmail = rootView.findViewById(R.id.etUEmail)
        etPassword = rootView.findViewById(R.id.etUPassword)
        etConfirmPassword = rootView.findViewById(R.id.etUCPassword)
        const = Const()

        //TO POPULATE DEPARTMENTS
        loadSpinnerData()

        btnAddCoordinator.setOnClickListener {
            addCoordinator()
        }

        spDepartment.onItemSelectedListener = this
        return rootView;
    }

    //TO LOAD DEPARTMENTS
    private fun loadSpinnerData() {
        val stringRequest = object : StringRequest(
            Request.Method.POST, const.DEPARTMENT_URL,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    try {
                        var jsonObject: JSONObject = JSONObject(response)
                        if(jsonObject.getString("log")=="success"){
                            var jsonArray: JSONArray = jsonObject.getJSONArray("department")
                            for (i in 0 until jsonArray.length()) {
                                val jsonObject1 = jsonArray.getJSONObject(i)
                                val name = jsonObject1.getString("dep_name")
                                departments.add(name)
                            }
                        }
                        spDepartment.setAdapter(
                            ArrayAdapter<String>(
                                rootView.context,
                                android.R.layout.simple_spinner_dropdown_item,
                                departments
                            )
                        )
                    } catch (e: Exception) {
                        Toast.makeText(activity,e.toString()+"#EXCEPTION#",Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(rootView.context,"Something went wrong...",Toast.LENGTH_SHORT).show()
                }
            }) {
        }
        val requestQueue = Volley.newRequestQueue(activity)
        requestQueue.add(stringRequest)
    }

    //TO ADD A NEW COORDINATOR USING WEB SERVICE
    private fun addCoordinator() {
        var username = etUName.text.toString()
        var email = etEmail.text.toString()
        var cpass = etConfirmPassword.text.toString()
        var password = etPassword.text.toString()
        if(checkPassword(cpass, password)){

        }
    }

    private fun checkPassword(cpass: String, password: String): Boolean {
        if(!cpass.equals(password)){
            tilCPassword.error = "Password not match"
        }
        return true
    }


    //SPINNER SELECTED LISTENER
    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        department = spDepartment.getItemAtPosition(position).toString()
    }

}
