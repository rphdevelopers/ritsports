package com.rphdevelopers.ritsports

import android.app.Activity
import android.os.Bundle
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import android.accounts.AccountManager.KEY_PASSWORD
import android.content.Intent
import android.content.Context
import android.view.View
import android.widget.*
import com.android.volley.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.json.JSONObject

class SignInActivity : Activity() {

    lateinit var etUsername: EditText
    lateinit var etPassword: EditText
    lateinit var btnSignIn: Button
    lateinit var btnClose: ImageButton
    lateinit var lprogressbar: View
    lateinit var const: Const

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        etUsername = findViewById(R.id.etUsername)
        etPassword = findViewById(R.id.etPassword)
        btnSignIn = findViewById(R.id.btnSignIn)
        btnClose = findViewById(R.id.btnCloseSignIn)

        lprogressbar = findViewById(R.id.llProgressBar)

        const = Const()

        btnSignIn.setOnClickListener { login() }
        btnClose.setOnClickListener { finish() }
    }

    private fun login() {
        val email = etUsername.text.toString()
        val password = etPassword.text.toString()

        //to show loading ....
        llProgressBar.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, const.LOGIN_URL,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    try {
                        val LOGIN_SUCCESS = "success"
                        val jsonObject: JSONObject = JSONObject(response)
                        if (jsonObject.getString("log").trim { it <= ' ' }.equals(LOGIN_SUCCESS, ignoreCase = true)) {
                            llProgressBar.visibility = View.GONE
                            val sharedPreferences = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
                            val editor = sharedPreferences.edit()
                            editor.putBoolean(const.LOGGEDIN_SHARED_PREF, true)
                            editor.putString(const.EMAIL_SHARED_PREF, email)
                            editor.putString(const.LOGGEDIN_ROLE_SHARED_PREF,jsonObject.getString("role"))
                            editor.commit()
                            finish()
                        } else {
                            llProgressBar.visibility = View.GONE
                            Toast.makeText(
                                applicationContext,
                                "Invalid username or password",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(applicationContext,"Something went wrong!",Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    llProgressBar.visibility = View.GONE
                }
            }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val prams = HashMap<String,String>()
                val KEY_EMAIL = "email"
                prams.put(KEY_EMAIL, email)
                prams.put(KEY_PASSWORD, password)
                return prams
            }
        }
        val requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(stringRequest)
    }
}
