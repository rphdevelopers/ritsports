package com.rphdevelopers.ritsports

import android.app.Dialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.navigation.NavigationView
import android.content.Intent
import android.content.SharedPreferences
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.icu.text.IDNA
import android.opengl.Visibility
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.appbar.AppBarLayout


class MainActivity : AppCompatActivity() {

    lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var headerView: View
    lateinit var dialog: Dialog
    lateinit var const: Const
    lateinit var navView: NavigationView
    lateinit var prefs: SharedPreferences
    lateinit var tvLogout: TextView
    lateinit var tvLogin: TextView
    lateinit var appbarLayout: AppBarLayout
    lateinit var imgCollaps: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appbarLayout = findViewById(R.id.appBarLayout)
        imgCollaps = findViewById(R.id.imgCollaps)
        navView = findViewById(R.id.nav_view)
        headerView = navView.getHeaderView(0)
        tvLogout = headerView.findViewById(R.id.tv_signout)
        tvLogin = headerView.findViewById(R.id.tv_signin)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        dialog = Dialog(applicationContext)
        const = Const()

        //TO CHECK USER LOGGED IN OR NOT
        setUser()

        //TO SET NAVIGATION DRAWER
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph,
            drawerLayout = findViewById(R.id.main_drawer_layout))
        toolbar.setupWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        //ON FRAGMENT CHANGE -- for pin the collapsing toolbar
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id != R.id.fragment_home) { //SET EXCEPT FOR THE HOME PAGE

            }else{
            }
        }

        //TO ADD ONCLICK FOR SIGN IN BUTTON IN DRAWER
        tvLogin.setOnClickListener {
            intent = Intent(this,SignInActivity::class.java)
            startActivity(intent)
        }
        //LOGOUT
        tvLogout.setOnClickListener {
            prefs = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
            val editor = prefs.edit()
            editor.putBoolean(const.LOGGEDIN_SHARED_PREF, false)
            editor.putString(const.LOGGEDIN_ROLE_SHARED_PREF, null)
            editor.commit()
            setUser()
        }
    }

    //TO SET THE LOGGED USER
    private fun setUser() {
        var menu: Int = R.menu.nav_menu

        prefs = getSharedPreferences(const.SHARED_PREF_NAME, Context.MODE_PRIVATE)
        if (prefs.getBoolean(const.LOGGEDIN_SHARED_PREF, false)) { //user logged in before
            setLogOutButton()            //TO VIEW LOGOUT BUTTON AND HIDE LOGIN
            val role: String = prefs.getString(const.LOGGEDIN_ROLE_SHARED_PREF,null).toString()
            when(role){
                const.ROLE_ADMIN -> menu = R.menu.nav_admin_menu
                else ->{menu = R.menu.nav_menu}
            }
            setNavMenu(menu)
        } else {
            setLogInButton()            //TO VIEW LOGIN BUTTON AND HIDE LOGOUT
            Toast.makeText(applicationContext,"not loged",Toast.LENGTH_SHORT).show()
            setNavMenu(menu)
        }
    }

    private fun setLogInButton() {
        Toast.makeText(applicationContext,"login",Toast.LENGTH_SHORT).show()
        tvLogin.visibility = View.VISIBLE
        tvLogin.isClickable = true

        tvLogout.isClickable = false
        tvLogout.visibility = View.GONE
    }

    private fun setLogOutButton() {
        Toast.makeText(applicationContext,"logout now",Toast.LENGTH_SHORT).show()
        tvLogout.visibility = View.VISIBLE
        tvLogout.isClickable = true

        tvLogin.isClickable = false
        tvLogin.visibility = View.GONE
    }

    private fun setNavMenu(menu: Int) {
        navView.menu.clear()
        navView.inflateMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        setUser()
    }
    //TO CHANGE FRAGMENT ON MENU ITEM CLICK
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


}
